#include <bits/stdc++.h>
using namespace std;
// constructor of Node
class Node
{
public:
    int val;
    Node *next;
    Node(int val)   //constructor
    {
        this->val = val;
        this->next = NULL;
    }
};

int main()
{
    Node a(10);
    Node b(20);

    a.next=&b;

    cout << a.val << endl
         << b.val << endl;         // manual access the value
    cout << a.next->val << endl;   // access value by dereference
    cout << (*a.next).val << endl; // another way to dereference

    return 0;
}