#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

int main()
{
    Node *head = new Node(10);
    Node *a = new Node(20);
    Node *b = new Node(30);
    Node *c = new Node(40);
    Node *d = new Node(50);

    head->next = a;
    a->next = b;
    b->next = c;
    c->next = d;

    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->val << endl;
        tmp = tmp->next;
    }
    tmp=head;
    while (tmp != NULL)
    {
        cout << tmp->val << endl;
        tmp = tmp->next;
    }

    // This is not also not wise way. when head go to Null for exceuting this loop, He never be an integer
    //  while (head!=NULL)
    //  {
    //      cout<<head->val<<endl;
    //      head=head->next;
    //  }

    /* Not Right way to print
        cout<<head->val<<endl;  //print 10
        cout<<head->next->val<<endl;    //print 20
        cout<<head->next->next->val<<endl;  //print 30
        cout<<head->next->next->next->val<<endl;    //print 40
        cout<<head->next->next->next->next->val<<endl; //print 50
    */
    return 0;
}