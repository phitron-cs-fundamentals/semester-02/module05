#include<bits/stdc++.h>
using namespace std;

class Node
{
    public:
    int val;
    Node* next;
};

int main()
{
    Node a,b;
    a.val = 10;     // manual value input
    b.val = 20;

    a.next=&b;   //linked list input
    b.next=NULL;

    cout<<a.val<<endl<<b.val<<endl; //manual access the value
    cout<<a.next->val<<endl;  //access value by dereference
    cout<<(*a.next).val<<endl;  //another way to dereference

    return 0;
}